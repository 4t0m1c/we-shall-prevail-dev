﻿using UnityEngine;
using System.Collections;


public class Spawner : MonoBehaviour {
	
	private Vector3 CurrPos;
	private Vector3 StartPos;
    private int CounterTotal;
    private int Counter;
	private int CounterUp;
	private bool finished;
	public GameObject Model;

	// Use this for initialization
	void Start () {
		CurrPos = this.transform.position;
		StartPos = this.transform.position;
		Counter = 0;
        StartCoroutine(Example());
    }

    IEnumerator Example()
    {
        while (!finished)
        {
            if (CounterUp == 46)
            {
                finished = true;
            }
            else
            {
                CounterTotal++;
                Counter++;
                Debug.Log("Counter:" + Counter);
                if (Counter == 46)
                {
                    CurrPos = new Vector3(StartPos.x, StartPos.y, StartPos.z + 1);
                    StartPos = new Vector3(StartPos.x, StartPos.y, CurrPos.z);
                    Debug.Log("CurrPos:" + CurrPos);
                    Counter = 0;
                    CounterUp++;
                }
                CurrPos = new Vector3(CurrPos.x + 1, CurrPos.y, CurrPos.z);
                Instantiate(Model, CurrPos, Quaternion.identity);
                Debug.Log("CounterUp:" + CounterUp);
            }
            yield return new WaitForSeconds(1);
        }
    }

    void Generate () {
		while (!finished){
			if (CounterUp == 46){
				finished = true;}
			else {
				Counter++;
				//Debug.Log ("Counter:" + Counter);
				if (Counter == 46) {
					CurrPos = new Vector3 (StartPos.x, StartPos.y, StartPos.z + 1);
					StartPos = new Vector3 (StartPos.x, StartPos.y, CurrPos.z);
					//Debug.Log ("CurrPos:" + CurrPos);
					Counter = 0;
					CounterUp++;
				}
				CurrPos = new Vector3 (CurrPos.x+1,CurrPos.y,CurrPos.z);
				Instantiate (Model,CurrPos,Quaternion.identity);
				//Debug.Log ("CounterUp:" + CounterUp);
			}
			//yield new WaitForSeconds (0.5f);
		}
	}

    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(1, 10, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(0.0f, 0.0f, 1f, 0.5f);
        string text = CounterTotal.ToString();
        GUI.Label(rect, text, style);
    }

    // Update is called once per frame
    void Update () {
		//Generate ();
	}
}
